package com.itau.controletarefas.web;

import com.itau.controletarefas.persistence.Task;
import com.itau.controletarefas.service.TaskService;
import com.itau.controletarefas.web.DTO.TaskPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskController {
    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/projeto/{projectId}/tarefa")
    public List<TaskPayload> getAllByProject(@PathVariable int projectId){
        List<TaskPayload> responses = new ArrayList<>();

        for(Task task : taskService.getAllByProject(projectId)){
            responses.add(new TaskPayload(task));
        }

        return responses;
    }

    @PostMapping("/projeto/{projectId}/tarefa")
    public TaskPayload create(@PathVariable int projectId, @RequestBody TaskPayload payload) {
        Task task = payload.buildEntity();
        task.setProjectById(projectId);

        return new TaskPayload(taskService.create(task));
    }
}

