package com.itau.controletarefas.service;

import com.itau.controletarefas.persistence.Task;
import com.itau.controletarefas.persistence.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    private TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getAllByProject(int projectId){
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task create(Task task){
        return taskRepository.save(task);
    }
}
